#include "../include/bvh.hpp"

/**
 * Acceleration Structure (BVH)
 */

BVH::BVH() {
    depth = 0;
    root = new AABB();
}

BVH::BVH(int d) {
    depth = d;
    root = new AABB();
}

BVH::~BVH() {
    
}
 
void BVH::make(Scene& scn, int depth) {
	//If depth is 0, start by making first box
		//Initialize values so that box starts with every object
    if(depth == 1) {
        printf("Depth 1\n");
    }
	if(depth == 0) {
        Vector botleft, topright;
        findBoundingVerts(scn, botleft, topright);
        box->setMin(botleft);
        box->setMax(topright);
        box->setSpheres(scn.getSpheres());
        box->setTriangles(scn.getTriangles());
        printf("Make %d\n", depth+1);
        depth++;
	}
    if(box->getObjectNum() < 2) {
        return;
    }
	//Find Longest dimention and split along that
		//Loop through each object in previous box
		//Figure out which box they belong in, add it
	Vector bmin = box->getMin();
	Vector bmax = box->getMax();
	//char axis = findLongestAxis(vrts);
	char axis = findLongestAxis(bmin, bmax);
	Vector w = bmax - bmin;
	AABB* s1 = new AABB(box);
	AABB* s2 = new AABB(box);
	float hp;
	switch(axis) {
		case 0: //X
			hp = w.x / 2.0;
			///Sub Box 1 (Left)
			s1->setMin(bmin);
			s1->setMax(Vector(bmax.x-hp, bmax.y, bmax.z));
			///Sub Box 2 (Right)
			s2->setMin(Vector(bmin.x+hp, bmin.y, bmin.z));
			s2->setMax(bmax);
			break;
		case 1: //Y
			hp = w.y / 2.0;
			///Sub Box 1 (Top)
			s1->setMin(Vector(bmin.x, bmin.y+hp, bmin.z));
			s1->setMax(bmax);
			///Sub Box 2 (Bottom)
			s2->setMin(bmin);
			s2->setMax(Vector(bmax.x, bmax.y-hp, bmax.z));
			break;
		case 2: //Z
			hp = w.z / 2.0;
			///Sub Box 1 (Front)
			s1->setMin(bmin);
			s1->setMax(Vector(bmax.x, bmax.y, bmax.z-hp));
			///Sub Box 2 (Back)
			s2->setMin(Vector(bmin.x, bmin.y, bmin.z+hp));
			s2->setMax(bmax);
			break;
	}
	//Find which box objects are in 
    std::vector<Sphere> sphs = box->getSpheres();
	int num = sphs.size();
	for(int i = 0; i < num; i++) {
		Sphere sphere = sphs[i];
		if(s1->isInBox(sphere)) {
			s1->addSphere(sphere);
		}
		if(s2->isInBox(sphere)) {
			s2->addSphere(sphere);
		}
	}
    std::vector<Triangle> tris = box->getTriangles();
	num = tris.size();
    if(depth == 1)
        printf("Tri Num: %d\n\n", num);
	for(int i = 0; i < num; i++) {
		Triangle triangle = tris[i];
		if(s1->isInBox(triangle)) {
			s1->addTriangle(triangle);
            if(depth == 1) {
                printf("Triangle %d in s1\n", i);
                printf("New Size: %d\n", (int)s1->getTriangles().size());
            }
		}
		if(s2->isInBox(triangle)) {
			s2->addTriangle(triangle);
            if(depth == 1) {
                printf("Triangle %d in s2\n", i);
                printf("New Size: %d\n", (int)s1->getTriangles().size());
            }
		}
	}
	box->setLeftChild(s1);
	box->setRightChild(s2);
	//If depth > bvhdepth
	//return. Don't do recursive calls
	if(depth == scn.getBVHDepth()) {
		return;
	}
	//Recursively call this function with the 2 new subboxes
	//Don't recurse on boxes with 0-2 items in it
	if(s1->getObjectNum() > 2) {
		make(scn, s1, depth+1);
	}
	if(s2->getObjectNum() > 2) {
		make(scn, s2, depth+1);
	}
}

Intersect* BVH::intersectBVH(const Ray& trace, double dmin, double dmax) const {
	Intersect* in = 0;
	AABB* bvh = root;
	//Vector min = bhv->min, max - bvh->max;
	//Vector orig = trace->p, dir = trace->d;
	//Check if intersects bvh
		//If subboxes are null, check intersection of objects
		//Else recurse down non-null boxes
	if(intersect(trace, bvh, dmin, dmax)) {
		//Leaf. Check intersections
		if(bvh->isLeaf()) {
			//printf("Leaf Box\n");
			Intersect* tmpSph = intersectSpheres(trace, bvh->getSpheres(), dmin, dmax);
			Intersect* tmpTri = intersectTriangle(trace, bvh->getTriangles(), dmin, dmax);
			if(tmpSph != 0) {
				in = new Intersect(tmpSph->normal, tmpSph->r, tmpSph->dist, tmpSph->p, tmpSph->m);
                delete tmpSph;
                tmpSph = 0;
			}
            if(tmpTri != 0) {
                if(in == 0) {
                    in = new Intersect(tmpTri->normal, tmpTri->r, tmpTri->dist, tmpTri->p, tmpTri->m);
                    delete tmpTri;
                    tmpTri = 0;
                }
                else if(in->dist > tmpTri->dist) {
                    in->normal = tmpTri->normal;
                    in->r = tmpTri->r;
                    in->dist = tmpTri->dist;
                    in->p = tmpTri->p;
                    in->m = tmpTri->m;
                    delete tmpTri;
                    tmpTri = 0;
                }
            }
		}
		//Recurse Down the subboxes
		else {
			Intersect* s1 = 0;
			//printf("Box1\n");
			if(bvh->getLeftChild() != 0) {
				s1 = intersectBVH(trace, bvh->getLeftChild(), dmin, dmax);
			}
            if(s1 != 0) {
                if(in == 0) {
                    in = new Intersect(s1->normal, s1->r, s1->dist, s1->p, s1->m);
                    delete s1;
                    s1 = 0;
                }
                else {
                    printf("I don't know what's happending\n");
                }
            }
			//printf("Box2\n");
			if(bvh->getRightChild() != 0) {
				s1 = intersectBVH(trace, bvh->getRightChild(), dmin, dmax);
			}
            if(s1 != 0) {
                if(in == 0) {
                    in = new Intersect(s1->normal, s1->r, s1->dist, s1->p, s1->m);
                    delete s1;
                    s1 = 0;
                }
                else if(in->dist > s1->dist) {
                    in->normal = s1->normal;
					in->r = s1->r;
					in->dist = s1->dist;
					in->p = s1->p;
					in->m = s1->m;
                    delete s1;
                    s1 = 0;
                }
            }
		}
	}
	return in;
}

void BVH::findBoundingVerts(const Scene& scn, Vector& bl, Vector& tr) {
	Vector max, min;
	bool init = false;
	Sphere s;
	std::vector<Sphere> sph = scn.getSpheres();
	int num = sph.size();
	for(int i = 0; i < num; i++) {
		s = sph[i];
		Vector pnt = s.getPosition() + s.getRadius();
		if(!init) {
			max.x = pnt.x;
			max.y = pnt.y;
			max.z = pnt.z;
			//printf("Max Init: (%f %f %f)\n", pnt.x, pnt.y, pnt.z);
		}
		else {
			if(max.x < pnt.x) {
				max.x = pnt.x;
			}
			if(max.y < pnt.y) {
				max.y = pnt.y;
			}
			if(max.z < pnt.z) {
				max.z = pnt.z;
			}
		}
		pnt = s.getPosition() - s.getRadius();
		if(!init) {
			min.x = pnt.x;
			min.y = pnt.y;
			min.z = pnt.z;
			init = true;
			//printf("Min Init: (%f %f %f)\n", pnt.x, pnt.y, pnt.z);
		}
		else {
			if(min.x > pnt.x) {
				min.x = pnt.x;
			}
			if(min.y > pnt.y) {
				min.y = pnt.y;
			}
			if(min.z > pnt.z) {
				min.z = pnt.z;
			}
		}
	}
	
	Triangle t;
	std::vector<Triangle> tri = scn.getTriangles();
	num = tri.size();
	for(int i = 0; i < num; i++) {
		t = tri[i];
		for(int j = 0; j < 3; j++) {
			Vector v = t.getVertex(j);
			if(!init) {
				max = v;
				min = v;
				init = true;
			}
			else {
				if(max.x < v.x) {
					max.x = v.x;
				}
				if(max.y < v.y) {
					max.y = v.y;
				}
				if(max.z < v.z) {
					max.z = v.z;
				}
				if(min.x > v.x) {
					min.x = v.x;
				}
				if(min.y > v.y) {
					min.y = v.y;
				}
				if(min.z > v.z) {
					min.z = v.z;
				}
			}
		}	
	}
	bl = min;
	tr = max;
    
	//Rectangle r;
	//std::vector<Rectangle> rec = scn->rectangles;
	//num = rec.size();
	//for(int i = 0; i < num; i++) {
		//r = rec[i];
	//}
}

//std::array<Vector, 8> vrts
char BVH::findLongestAxis(const Vector& vmin, const Vector& vmax) {
	Vector w = vmax - vmin;
	w = Vector((float)fabs(w.x), (float)fabs(w.y), (float)fabs(w.z));
	if(w.x >= w.y && w.x >= w.z) {
		return 0;
	}
	if(w.y >= w.x && w.y >= w.z) {
		return 1;
	}
	if(w.z >= w.x && w.z >= w.y) {
		return 2;
	}
	return -1;
}

//An Efficient and Robust Ray-Box Intersection Algorithm
bool BVH::intersect(const Ray& trace, const AABB* box, double dmin, double dmax) const {
	Vector max = box->getMax(), min = box->getMin();
	Vector orig = trace.getPosition(), dir = trace.getDirection();
    //If origin is inside the box, return true
    if((orig.x >= min.x && orig.x <= max.x) && (orig.y >= min.y && orig.y <= max.y) && 
       (orig.z >= min.z && orig.z <= max.z)) {
        return true;
    }
    float tmin, tmax, tymin, tymax, tzmin, tzmax, div;
    ///X
    div = 1 / dir.x;
    if(div >= 0) {
        tmin = (min.x - orig.x) * div;
        tmax = (max.x - orig.x) * div;
    }
    else {
        tmin = (max.x - orig.x) * div;
        tmax = (min.x - orig.x) * div;
    }
    ///Y
    div = 1 / dir.y;
    if(div >= 0) {
        tymin = (min.y - orig.y) * div;
        tymax = (max.y - orig.y) * div;
    }
    else {
        tymin = (max.y - orig.y) * div;
        tymax = (min.y - orig.y) * div;
    }
    if((tmin > tymax) || (tymin > tmax)) {
        return false;
    }
    if(tymin > tmin) {
        tmin = tymin;
    }
    if(tymax < tmax) {
        tmax = tymax;
    }
    ///Z
    div = 1 / dir.z;
    if(div >= 0) {
        tzmin = (min.z - orig.z) * div;
        tzmax = (max.z - orig.z) * div;
    }
    else {
        tzmin = (max.z - orig.z) * div;
        tzmax = (min.z - orig.z) * div;
    }
    if((tmin > tzmax) || (tzmin > tmax)) {
        return false;
    }
    if(tzmin > tmin) {
        tmin = tzmin;
    }
    if(tzmax < tmax) {
        tmax = tzmax;
    }
    return (tmin < dmax) && (tmax > dmin);
    /** I'm unsure where this came from. It's not in the paper anywhere.
    double dist;
    Vector distv;
    if(tmin >= 0) {
        distv = orig + (dir * tmin);
        dist = distv.magnitudeSq();
    }
    else if(tmax >= 0) {
        distv = orig - (orig + (dir * tmax));
        dist = distv.magnitudeSq();
    }
    return ((dist < dmax) && (dist > dmin));
    */
}

//Helper function for recursive printing
void BVH::print() {
	printBVH(root, 0);
}

void BVH::printBVH(const AABB* b, int depth) {
	for(int i = 0; i < depth; i++) {
		printf("--");
	}
    Vector min = b->getMin();
    Vector max = b->getMax();
    std::vector<Sphere> sphs = b->getSpheres();
	printf("Min(%f %f %f), Max(%f %f %f)\n", min.x, min.y, min.z, max.x, max.y, max.z);
	for(int j = 0; j < (signed)sphs.size(); j++) {
		for(int i = 0; i < depth; i++) {
			printf("  ");
		}
		printf("Sphere:\n");
		for(int i = 0; i < depth; i++) {
			printf("  ");
		}
		printf("Pos: (%f %f %f)\n", sphs[j].getPosition().x, sphs[j].getPosition().y, sphs[j].getPosition().z);
		for(int i = 0; i < depth; i++) {
			printf("  ");
		}
		printf("Rad: %f\n", sphs[j].getRadius());
	}
    std::vector<Triangle> t = b->getTriangles();
	for(int j = 0; j < (signed)t.size(); j++) {
		for(int i = 0; i < depth; i++) {
			printf("  ");
		}
		printf("Triangle:\n");
		for(int i = 0; i < depth; i++) {
			printf("  ");
		}
		printf("V1: (%f %f %f)\n", t[j].getVertices()[0].x, t[j].getVertices()[0].y, t[j].getVertices()[0].z);
		for(int i = 0; i < depth; i++) {
			printf("  ");
		}
		printf("V2: (%f %f %f)\n", t[j].getVertices()[1].x, t[j].getVertices()[1].y, t[j].getVertices()[1].z);
		for(int i = 0; i < depth; i++) {
			printf("  ");
		}
		printf("V3: (%f %f %f)\n", t[j].getVertices()[2].x, t[j].getVertices()[2].y, t[j].getVertices()[2].z);
	}
    /*
	for(int j = 0; j < (signed)b->planes.size(); j++) {
		std::vector<Plane> p = b->planes;
		for(int i = 0; i < depth; i++) {
			printf("  ");
		}
		printf("Plane:\n");
		for(int i = 0; i < depth; i++) {
			printf("  ");
		}
		printf("Point: (%f %f %f)\n", p[j].point.x, p[j].point.y, p[j].point.z);
		for(int i = 0; i < depth; i++) {
			printf("  ");
		}
		printf("Normal: (%f %f %f)\n", p[j].normal.x, p[j].normal.y, p[j].normal.z);
	}
	*/
	if(b->getLeftChild() != 0) {
		printBVH(b->getLeftChild(), depth+1);
	}
	if(b->getRightChild() != 0) {
		printBVH(b->getRightChild(), depth+1);
	}
}

/********************
 * AABB
 ********************/
AABB::AABB() {
    left = right = parent = 0;
    min = max = Vector();
}

AABB::AABB(const Vector& minimum, const Vector& maximum) {
    left = right = parent = 0;
    min = minimum;
    max = maximum;
}

AABB::AABB(AABB* p) {
    left = right = 0;
    parent = p;
    //parent = new AABB(p.getSpheres(), p.getTriangles());
    //parent->setMin(p.getMin());
    //parent->setMax(p.getMax());
    //parent->setParent(p.getParent());
    //parent->setLeftChild(p.getLeftChild());
    //parent->setRightChild(p.getRightChild());
}

AABB::AABB(const std::vector<Sphere> sphs, const std::vector<Triangle> tris) {
    spheres = sphs;
    triangles = tris;
    left = right = parent = 0;
}

//http://www.mrtc.mdh.se/projects/3Dgraphics/paperF.pdf
//On Faster Sphere-Box Overlap Testing
bool AABB::isInBox(const Sphere& sph) const {
    double d = 0, e, rad = sph.getRadius();
	Vector spos = sph.getPosition();
	//X
	if((e = spos.x - min.x) < 0) {
		if(e < -rad) {
			return false;
		}
		d += e*e;
	}
	else if((e = spos.x - max.x) > 0) {
		if(e > rad) {
			return false;
		}
		d += e*e;
	}
	//Y
	if((e = spos.y - min.y) < 0) {
		if(e < -rad) {
			return false;
		}
		d += e*e;
	}
	else if((e = spos.y - max.y) > 0) {
		if(e > rad) {
			return false;
		}
		d += e*e;
	}
	//Z
	if((e = spos.z - min.z) < 0) {
		if(e < -rad) {
			return false;
		}
		d += e*e;
	}
	else if((e = spos.z - max.z) > 0) {
		if(e > rad) {
			return false;
		}
		d += e*e;
	}
	
	return (d <= (rad*rad));
}

//http://fileadmin.cs.lth.se/cs/Personal/Tomas_Akenine-Moller/pubs/tribox.pdf
//Fast 3D Triangle-Box Overlap Testing
bool AABB::isInBox(const Triangle& tri) const {
	Vector v0, v1, v2, e0, e1, e2, normal;
    float fex, fey, fez, fmin, fmax, p0, p1, p2, rad;
    Vector boxhalfsize = (max - min) / 2.0;
    Vector bc = min + boxhalfsize;
    std::vector<Vector> verts = tri.getVertices();
    v0 = verts[0] - bc;
    v1 = verts[1] - bc;
    v2 = verts[2] - bc;
    
    e0 = v1 - v0;
    e1 = v2 - v1;
    e2 = v0 - v2;
    
    //Bullet 3
    fex = fabs(e0.x);
    fey = fabs(e0.y);
    fez = fabs(e0.z);
    AXISTEST_X01(e0.z, e0.y, fez, fey);
    AXISTEST_Y02(e0.z, e0.x, fez, fex);
    AXISTEST_Z12(e0.y, e0.x, fey, fex);
    
    fex = fabs(e1.x);
    fey = fabs(e1.y);
    fez = fabs(e1.z);
    AXISTEST_X01(e1.z, e1.y, fez, fey);
    AXISTEST_Y02(e1.z, e1.x, fez, fex);
    AXISTEST_Z0(e1.y, e1.x, fey, fex);
    
    fex = fabs(e2.x);
    fey = fabs(e2.y);
    fez = fabs(e2.z);
    AXISTEST_X2(e2.z, e2.y, fez, fey);
    AXISTEST_Y1(e2.z, e2.x, fez, fex);
    AXISTEST_Z12(e2.y, e2.x, fey, fex);
    
    //Bullet 1
    findMinMax(v0.x, v1.x, v2.x, fmin, fmax);
    if(fmin > boxhalfsize.x || fmax < -boxhalfsize.x) {
        return false;
    }
    
    findMinMax(v0.y, v1.y, v2.y, fmin, fmax);
    if(fmin > boxhalfsize.y || fmax < -boxhalfsize.y) {
        return false;
    }
    
    findMinMax(v0.z, v1.z, v2.z, fmin, fmax);
    if(fmin > boxhalfsize.z || fmax < -boxhalfsize.z) {
        return false;
    }
    
    //Bullet 2
    normal = Vector::cross(e0, e1);
    Plane pl;
    pl.point = v0;
    pl.normal = normal;
    if(!isInBox(pl)) {
        return false;
    }
    
    return true;
}

//Also From Triangle-Box Testing
bool AABB::isInBox(const Plane& pln) const {
	Vector normal = pln.normal, vert = pln.point, maxbox = max;
	Vector vmin, vmax; 
    float v;
    
    v = vert.x;
    if(normal.x>0.0f) {
      vmin.x = -maxbox.x - v;
      vmax.x = maxbox.x - v;
    }
    else {
      vmin.x = maxbox.x - v;
      vmax.x = -maxbox.x - v;
    }
    
    if(normal.y>0.0f) {
      vmin.y = -maxbox.y - v;
      vmax.y = maxbox.y - v;
    }
    else {
      vmin.y = maxbox.y - v;
      vmax.y = -maxbox.y - v;
    }
    
    if(normal.z>0.0f) {
      vmin.z = -maxbox.z - v;
      vmax.z = maxbox.z - v;
    }
    else {
      vmin.z = maxbox.z - v;
      vmax.z = -maxbox.z - v;
    }
    
    if(Vector::dot(normal, vmin) > 0.0f) {
        return false;
    }
    if(Vector::dot(normal, vmax) >= 0.0f) {
        return true;
    }
    return false;
}

void AABB::findMinMax(float x0, float x1, float x2, float& min, float& max) const {
	min = max = x0;
	if(x1<min) min=x1;
	if(x1>max) max=x1;
	if(x2<min) min=x2;
	if(x2>max) max=x2;
}
